import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicStorageModule } from '@ionic/storage';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ContactsPage } from '../pages/contacts/contacts';
import { ContactDetailsPage } from '../pages/contact-details/contact-details';
import { EventsPage } from '../pages/events/events';
import { MedicationPage } from '../pages/medication/medication';
import { DietPage } from '../pages/diet/diet';
import { ProfilePage } from '../pages/profile/profile';
import { ModalAddMealPage } from '../pages/modal-add-meal/modal-add-meal';
import { ModalAddContactPage } from '../pages/modal-add-contact/modal-add-contact';
import { DashboardPage } from '../pages/dashboard/dashboard';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    ContactsPage,
    ContactDetailsPage,
    EventsPage,
    MedicationPage,
    DietPage,
    ProfilePage,
    ModalAddMealPage,
    ModalAddContactPage,
    DashboardPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HelloIonicPage,
    ContactsPage,
    ContactDetailsPage,
    EventsPage,
    MedicationPage,
    DietPage,
    ProfilePage,
    ModalAddMealPage,
    ModalAddContactPage,
    DashboardPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
