import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ContactDetailsPage } from '../contact-details/contact-details';
import { ModalAddContactPage } from '../modal-add-contact/modal-add-contact';

/**
 * Generated class for the ContactsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {
  searchQuery: string = '';
  contacts: Array<{name: string, number: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.initializeItems();
  }

  initializeItems() {
    this.contacts = [
      {name: 'John Doe', number: '7777777'},
      {name: 'Jane Doe', number: '9999999'},
      {name: 'Alex Smith', number: '7888888'},
    ];
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.contacts = this.contacts.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  itemTapped(event, item) {
    this.navCtrl.push(ContactDetailsPage, {
      item: item
    });
  }

  openModal() {
    let myModal = this.modalCtrl.create(ModalAddContactPage);
    myModal.onDidDismiss(data => {
      let con = {
        name: data.name,
        number: data.number
      }
     this.contacts.push(con)
    });
    myModal.present();
  }

}
