import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {
  events: Array<{name: string, description: string}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.events = [
      {name: 'Appointment', description: 'You have an appointment on 11/01/18'},
      {name: 'Follow Up', description: 'You have a follow up on 12/01/18'},
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsPage');
  }

}
