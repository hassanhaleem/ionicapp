import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the ModalAddContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-add-contact',
  templateUrl: 'modal-add-contact.html',
})
export class ModalAddContactPage {
  // private todo : FormGroup;
todo = { name:null, number: null}
  constructor(public toastCtrl: ToastController, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
    // this.todo = this.formBuilder.group({
    //       name: ['', Validators.required],
    //       number: [''],
    //     });
  }

  logForm(){
    this.viewCtrl.dismiss(this.todo);
    this.presentToast();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalAddContactPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Contact was added successfully',
      duration: 3000
    });
    toast.present();
  }

}
