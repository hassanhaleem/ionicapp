import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAddContactPage } from './modal-add-contact';

@NgModule({
  declarations: [
    ModalAddContactPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAddContactPage),
  ],
})
export class ModalAddContactPageModule {}
