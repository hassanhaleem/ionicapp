import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DietPage } from '../diet/diet';
import { ContactsPage } from '../contacts/contacts';
import { EventsPage } from '../events/events';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  goDiet(){
    this.navCtrl.push(DietPage);
  }

  goContact(){
    this.navCtrl.push(ContactsPage);
  }

  goEvent(){
    this.navCtrl.push(EventsPage);
  }

}
