import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MedicationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-medication',
  templateUrl: 'medication.html',
})
export class MedicationPage {
  searchQuery: string = '';
  medications: Array<{name: string, strength: string, description: string}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeItems();
  }

  initializeItems() {
    this.medications = [
      {name: 'Azithromycin', strength: '250 mg', description: 'Medicine Description...'},
      {name: 'Ivacaftor', strength: '150 mg', description: 'Medicine Description...'},
      {name: 'Panadol', strength: '122 mg', description: 'Medicine Description...'},
    ];
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicationPage');
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.medications = this.medications.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
