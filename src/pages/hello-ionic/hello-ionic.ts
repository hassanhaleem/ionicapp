import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
  // private todo : FormGroup;
  todo = { name: null, password: null}
  constructor(public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
    // this.todo = this.formBuilder.group({
    //       name: ['', Validators.required],
    //       password: ['', Validators.required],
    //     });
  }

  checkUser(){
    if(((this.todo.name == 'hassan') || (this.todo.name == 'Hassan')) && (this.todo.password == '123456')){
      this.navCtrl.setRoot(DashboardPage);
      this.presentToast();
    }
    else {
      this.loginFail();
    }

  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Logged In Successfully',
      duration: 3000
    });
    toast.present();
  }

  loginFail() {
    let toast = this.toastCtrl.create({
      message: 'Incorrect Username or Password',
      duration: 3000
    });
    toast.present();
  }
}
