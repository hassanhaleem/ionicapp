import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the ModalAddMealPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-add-meal',
  templateUrl: 'modal-add-meal.html',
})
export class ModalAddMealPage {
  private todo : FormGroup;

  constructor(public toastCtrl: ToastController, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
    this.todo = this.formBuilder.group({
          name: ['', Validators.required],
          calory: [''],
        });
  }

  logForm(){
    this.viewCtrl.dismiss();
    this.presentToast();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalAddMealPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Meal was added successfully',
      duration: 3000
    });
    toast.present();
  }

}
