import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAddMealPage } from './modal-add-meal';

@NgModule({
  declarations: [
    ModalAddMealPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAddMealPage),
  ],
})
export class ModalAddMealPageModule {}
