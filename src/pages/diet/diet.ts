import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ModalAddMealPage } from '../modal-add-meal/modal-add-meal';

/**
 * Generated class for the DietPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-diet',
  templateUrl: 'diet.html',
})
export class DietPage {
  meals: Array<{name: string, calory: string, img: string}>;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.meals = [
      {name: 'Strawberry Banana Yogurt', calory: '154', img: 'meal1'},
      {name: 'Banana Breakfast', calory: '132', img: 'meal2'},
      {name: 'Honey-Pecan Kiwi Salad', calory: '72', img: 'meal3'},
      {name: 'Potato and Bean Sandwich', calory: '132', img: 'meal4'},
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DietPage');
  }

  openModal() {
    let myModal = this.modalCtrl.create(ModalAddMealPage);
    myModal.present();
  }

}
